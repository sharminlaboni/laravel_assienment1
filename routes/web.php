<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/frontend', function () {
    return view('frontend.index');
});

Route::get('/about', function () {
    return view('frontend.about');
});
Route::get('/testimonial', function () {
    return view('frontend.testimonial');
});




Route::get('/contact', function () {
    return view('frontend.contact');
});

Route::get('/products', function () {
    return view('frontend.products');
});

Route::get('/blog_list', function () {
    return view('frontend.blog_list');
});

  
Route::get('/dash', function () {
    return view('backend.dashboard');
});
Route::get('/user', function () {
    return view('backend.user');
});
Route::get('/catagory', function () {
    return view('backend.catagory');
});
Route::get('/order', function () {
    return view('backend.order');
});
Route::get('/customer', function () {
    return view('backend.customer');
});
Route::get('/delivery', function () {
    return view('backend.delivery');
});

Route::get('/product', function () {
    return view('backend.product');
});

